"use strict";

describe('Simple search test', function() {
  var mainPage = require('../models/pages/MainPage');
  var resultSearchPanel;

  it('should display search result ', function() {
    mainPage.get();
    resultSearchPanel = mainPage.header.search('component');
    expect(resultSearchPanel.isSeaarchResultExist());
  });

  it('should hide result panel after clear', function() {
    mainPage.header.clearSearchInput();
    expect(!resultSearchPanel.isSeaarchResultExist());
  });
});

describe('QuickstartPage test', function() {
  var mainPage = require('../models/pages/MainPage');
  var quickstartPage;

  it('should open QuickstartPage', function() {
    mainPage.get();
    quickstartPage = mainPage.openQuickstartPage();
    expect(browser.getTitle()).toContain('Angular - Getting started');
  });

  it('drawer should hidden after button click test', function() {
    quickstartPage.header.clickDrawerButton();
    expect(!quickstartPage.drawer.isDraverVisible());
  });
});