var MainPage = function() {
  this.header = require('../elements/Header');
  this.getStartedButton = element(by.className('button hero-cta'));

  this.get = function() {
    browser.get('https://angular.io/');
  };

  this.openQuickstartPage = function() {
  	this.getStartedButton.click();
  	return require('./QuickstartPage');
  }
};
module.exports = new MainPage();