var Header = function() {
  const headerElement = element(by.className('app-toolbar'));	
  this.serachInput = headerElement.element(by.css('aio-search-box.search-container input'));
  this.drawerButton = element(by.className('hamburger mat-button'));

  this.search = function(data) {
    this.serachInput.sendKeys(data);
    return require('./SearchResult');
  };

  this.clearSearchInput = function() {
  	this.serachInput.clear();
  };

  this.clickDrawerButton = function(){
  	browser.wait(EC.visibilityOf(this.drawerButton), 5000);
  	this.drawerButton.click();
  };

};
module.exports = new Header();