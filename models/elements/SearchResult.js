var SearchResult = function() {
  const searchResultElement = element(by.tagName('aio-search-results'));
  this.searchArea = searchResultElement.element(by.className('search-area'));

  this.isSeaarchResultExist = function() {
  	try {
  		browser.wait(EC.visibilityOf(this.searchArea), 5000);
  	}
  	catch(e) {
  		return false;
  	}
  	return true;
  };

};
module.exports = new SearchResult();